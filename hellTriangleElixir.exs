
###############################################################################
# Modules
defmodule FileParser do
  def read(file) do
    {:ok, data} = File.read(file)
    data
    |> String.split("\n")
    |> parse
  end

  def parse(lines) do
    lineArr = Enum.map(Enum.drop(lines, 1),
      fn(item) -> String.split(item, ",") end)

    lines = [ [Enum.at(lines, 0)] | lineArr ]
    lines = to_int(lines)

    if  Enum.count(lines) > 0 && Enum.count(Enum.at(lines, -1)) == 0 do
      Enum.drop(lines, -1)
    else
      lines
    end
  end

  def to_int(res) do
    Enum.map(res, fn(line)->
      Enum.flat_map(line, fn item ->
        case Integer.parse(item) do
          {int, _rest} -> [int]
          :error -> []
        end
      end)
    end)
  end

end

defmodule Triangle do

  def maxItemLine(inputArray) do
     maxItemLine(inputArray, -1, [])
  end

  def maxItemLine([head | tail], lastIdx, acArr) do
    localArr = Enum.slice(head, lastIdx..lastIdx+1)
    itemLastIdx = Enum.max_by(localArr, fn(x) -> elem(x,0) end)
    acArr = acArr ++ [itemLastIdx]
    maxItemLine(tail, elem(itemLastIdx,1), acArr)
  end

  def maxItemLine([], lastIdx, acArr) do
    _lastIdx = lastIdx
    acArr
  end
end
###############################################################################

fileNamePath =
if Enum.count(System.argv)  > 0 do
  Enum.at(System.argv, 0)
else
  "inputArray.txt"
end

trig = FileParser.read(fileNamePath)
#
trigIdx = for x <- trig, do: Enum.with_index(x)
trigIdxPath = Triangle.maxItemLine(trigIdx);
trigIdxPath = elem(Enum.unzip(trigIdxPath),0)

IO.inspect trig
IO.inspect trigIdxPath

IO.puts Enum.sum(trigIdxPath)
